<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../../class/Spyc.class.php';
$config = Spyc::YAMLLoad('../../config/config.yaml');

$locations = array();
$projects = array();

$con = mysqli_connect(
    $config['mysql']['host'], 
    $config['mysql']['user'], 
    $config['mysql']['password'], 
    $config['mysql']['database']
) or die("Error connecting to database: ".mysqli_connect_error());

//get locations
$sql = "SELECT ID, Name FROM locations";
$select = mysqli_query($con, $sql) or die("Error: " . mysqli_error($con)."<br>$sql");
if (mysqli_num_rows($select) > 0) {
    while ($listrow = mysqli_fetch_array($select)) {
        $locations[$listrow['ID']] = $listrow['Name'];
    }
}

//get projects
if (isset($_GET['id'])) {
    $sql = "SELECT * FROM projects WHERE ID = " . $_GET['id'];
} else {
    $sql = "SELECT * FROM projects";
}
$select = mysqli_query($con, $sql) or die("Error: " . mysqli_error($con)."<br>$sql");
if (mysqli_num_rows($select) > 0) {
    while ($listrow = mysqli_fetch_array($select)) {
            $name = (isset($_GET['id'])) ? $listrow['Name'] : sprintf('<a href="project.php?%s">%s</a>', $listrow['ID'], $listrow['Name']);
        $projects[] = array(
            'id' => $listrow['ID'],
            'name' => $name,
            'progress' => $listrow['Progress']."%",
            'location' => $locations[$listrow['Location_ID']],
            'status' => ucfirst($listrow['Status']), 
            'before_image' => $listrow['Before_Image'], 
            'updated_at' => date('m/d/Y', strtotime($listrow['Updated_at']))
        );
    }
}
mysqli_close($con);

echo json_encode($projects);
