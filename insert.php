<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    require_once 'class/Spyc.class.php';
    $config = Spyc::YAMLLoad('config/config.yaml');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <title>Add a Project</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Brand</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="insert.php">Insert</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form action="insert-action.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Project Name</label>
                            <input class="form-control" type="text" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="locations">Location</label>
                            <select class="form-control" name="location_id">
                                <?php
                                    $con = mysqli_connect(
                                        $config['mysql']['host'], 
                                        $config['mysql']['user'], 
                                        $config['mysql']['password'], 
                                        $config['mysql']['database']
                                    ) or die("Error connecting to database: ".mysqli_connect_error());

                                    $sql = "SELECT ID, Name FROM locations";
                                    $select = mysqli_query($con, $sql) or die("Error: " . mysqli_error($con)."<br>$sql");
                                    if (mysqli_num_rows($select) > 0) {
                                        while ($listrow = mysqli_fetch_array($select)) {
                                            echo sprintf('<option value="%s">%s</option>', $listrow['ID'], $listrow['Name']);
                                        }
                                    }
                                    mysqli_close($con);
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" name="status">
                                <option value="new">New</option>
                                <option value="inprogress">In Progress</option>
                                <option value="complete">Complete</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="progress">Progress</label>
                            <input class="form-control" type="range" name="progress"  value="0">
                        </div>
                        <div class="form-group">
                            <label for="before_image">Before Image</label>
                            <input class="form-control" type="file" name="before_image" accept="image/*" >
                        </div>
                        <p>
                            <input class="btn btn-default" type="submit" value="Submit">
                        </p>
                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </body>
</html>