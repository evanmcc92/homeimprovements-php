<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once 'class/Spyc.class.php';
$config = Spyc::YAMLLoad('config/config.yaml');

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["before_image"]["name"]);
$uploadok = 1;
// Check if image file is a actual image or fake image
$imagefiletype = pathinfo($target_file, PATHINFO_EXTENSION);
$con = mysqli_connect(
    $config['mysql']['host'], 
    $config['mysql']['user'], 
    $config['mysql']['password'], 
    $config['mysql']['database']
) or die("Error connecting to database: ".mysqli_connect_error());

$sql = 'INSERT INTO projects SET ';

foreach ($_POST as $key => $value) {
    $sql .= sprintf('%s = "%s", ', $key, addslashes($value));
}

$check = getimagesize($_FILES["before_image"]["tmp_name"]);

if ($check !== false) {
    $uploadok = 1;
} else {
    $uploadok = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadok !== 0) {
    if (move_uploaded_file($_FILES["before_image"]["tmp_name"], $target_file)) {
        $sql .= sprintf('before_image = "%s", ', $_FILES["before_image"]["name"]);
    }
}

$sql = rtrim($sql, ', ');
$select = mysqli_query($con, $sql) or die("Error: " . mysqli_error($con)."<br>$sql");

mysqli_close($con);
header('Location: index.php');
